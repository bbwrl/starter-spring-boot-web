package ch.bbw.helloworld;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ui.Model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class TestMainController {

    @Test
    void testHomeRoute() {
        MainController mainController = new MainController();

        ModelMock mm = new ModelMock();
        mainController.home(mm, null);
        assertTrue(mm.getAttribute("message").equals("Hello"));

        mainController.home(mm, "Hugo");
        assertTrue(mm.getAttribute("message").equals("Hello Hugo"));
    }

}

class ModelMock implements Model {

    HashMap<String, Object> attributes = new HashMap<>();

    @Override
    public Model addAttribute(String s, Object o) {
        attributes.put(s,o);
        return this;
    }

    @Override
    public Model addAttribute(Object o) {
        return null;
    }

    @Override
    public Model addAllAttributes(Collection<?> collection) {
        return null;
    }

    @Override
    public Model addAllAttributes(Map<String, ?> map) {
        return null;
    }

    @Override
    public Model mergeAttributes(Map<String, ?> map) {
        return null;
    }

    @Override
    public boolean containsAttribute(String s) {
        return false;
    }

    @Override
    public Object getAttribute(String s) {
        return attributes.get(s);
    }

    @Override
    public Map<String, Object> asMap() {
        return null;
    }
}
